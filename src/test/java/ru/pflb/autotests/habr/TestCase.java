package ru.pflb.autotests.habr;

import org.testng.annotations.*;
import ru.pflb.autotests.habr.pages.*;

import java.lang.reflect.Method;

/**
 * @author Miroshnichenko Anatoliy.
 */
public class TestCase {

    private MainPage mainPage;
    private SetResultPage setResultPage;
    private LoginPage loginPage;
    private RegisterPage registerPage;
    private AdvertisingPage advertisingPage;

    @BeforeClass
    public void setUp(){
        mainPage = new MainPage();
        setResultPage = new SetResultPage();
        loginPage = new LoginPage();
        registerPage = new RegisterPage();
        advertisingPage = new AdvertisingPage();
    }

    @BeforeMethod
    public void beforeMethod(Method method){
        mainPage.log("Запуск теста: " + method.getName());
        mainPage.open();
    }

//    @Ignore
    @Test
    public void testCase1(){
        mainPage.openSearchField();
        mainPage.setSearchField("xpath");
        setResultPage.sortByRating();
        setResultPage.selectPage(4);
        setResultPage.selectMostRead(1);
        mainPage.clickLoginText();
        loginPage.setEmailField("email@notcorrect");
        loginPage.setPasswordField("4567");
        loginPage.isWarningEmail();
    }

//    @Ignore
    @Test
    public void testCase2(){
        mainPage.clickRegisterButton();
        registerPage.setEmailField("email@correct.ok");
        registerPage.setNicknameField("performance");
        registerPage.setPasswordField("12345678");
        registerPage.setPasswordRepeatField("12345678");
        registerPage.setCaptchaField("987654");
        registerPage.clickRegistrationButton();
        registerPage.isWarningCaptcha();
    }

    @Test
    public void testCase3() {
        mainPage.clickAdvertisingText();
        advertisingPage.isTextPresent("Медийная");
        advertisingPage.isTextPresent("реклама");
    }

    @AfterMethod
    public void afterMethod(Method method){
        mainPage.log("Конец теста: " + method.getName());
    }

    @AfterClass
    public void afterClass(){
        mainPage.closeAll();
    }
}
