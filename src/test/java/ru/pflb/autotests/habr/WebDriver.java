package ru.pflb.autotests.habr;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;

/**
 * @author Miroshnichenko Anatoliy.
 */
public class WebDriver {

    private static WebDriver webDriver = new WebDriver();
    private static ChromeDriver driver;
    public static Logger log;
    private static WebDriverWait wait;


    public static WebDriver getInstance(){
        return webDriver;
    }

    public void close(){
        log.trace("Закрываю последнее открытое окно");
        driver.close();
    }

    public void quit(){
        log.trace("Закрываю все окна");
        driver.quit();
    }
    private WebDriver(){
        log = LogManager.getLogger();
        System.setProperty("webdriver.chrome.driver", "bin/chromedriver.exe");
        ChromeOptions options = new ChromeOptions();
        options.addArguments("disable-infobars");
        driver = new ChromeDriver(options);
        driver.manage().window().maximize();
        wait = new WebDriverWait(driver,8,250);
    }

    public void get (String url){
        log.trace(String.format("Открываю сайт по адресу '%s'", url));
        driver.get(url);
    }

    public void isElementPresent(String xpath){
        log.trace(String.format("Проверяю наличие элемента '%s'",xpath));
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));
    }

    public void switchFocusToPage(String newUrl){
        String oldUrl = driver.getCurrentUrl();
        if(!oldUrl.equals(newUrl)) {
            log.trace(String.format("Меняю фокус драйвера на '%s'", newUrl));

            String oldDescriptor = driver.getWindowHandle(); //текущий дескриптор
            Iterator<String> windowIterator = driver.getWindowHandles().iterator();
            while (windowIterator.hasNext()) {
                String windowHandle = windowIterator.next();
                driver.switchTo().window(windowHandle); //меняем дескриптор

                String newDescriptor = driver.getWindowHandle();
                String currentUrl = driver.getCurrentUrl();
                if ((!oldDescriptor.equals(newDescriptor)) && currentUrl.equals(newUrl)){ //если дескриптор поменялся и URL дескриптора соответствует запросу
                    break;
                }
            }
        }
    }

    public WebElement findElementByXPath(String xpath){
        log.trace(String.format("Ищу элемент по локатору '%s'",xpath));
        WebElement element = null;
        try{ element = wait.until(
                ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));
            element = wait.until(
                    ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));
            element = wait.until(
                    ExpectedConditions.elementToBeClickable(By.xpath(xpath)));

//        ((JavascriptExecutor)driver).executeScript(
//                "arguments[0].scrollIntoView(true);",element);

            Actions scroll = new Actions(driver);
            scroll.moveToElement(element);
            scroll.perform();
        }
        catch (Exception e){
            log.trace("Создаю скриншот ошибки");
            File file =((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
            try {
                SimpleDateFormat simpleFormat = new SimpleDateFormat("dd.MM_HH-mm-ss");
                Date date = new Date();

                FileUtils.copyFile(file,new File(
                        String.format("screenshots/%s-scr.jpg", simpleFormat.format(date))));
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        return element;
    }
}
