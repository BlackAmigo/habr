package ru.pflb.autotests.habr.pages;

/**
 * @author Miroshnichenko Anatoliy.
 */
public class SetResultPage extends BasePage{

    public void sortByRating(){
        log("Сортирую результаты по рэйтингу");
        String ratingPath = "//ul[@class='toggle-menu']/li[3]";
        webDriver.findElementByXPath(ratingPath).click();
    }

    public void selectPage (int num){
        log(String.format("Перехожу на %s страницу", num));
        String pagePath = String.format("//ul[@id='nav-pagess']/li[%d]", num);
        webDriver.findElementByXPath(pagePath).click();
    }

    public void selectMostRead (int num){
        log(String.format("Перехожу по %s ссылке", num));
        String mostReadPath = String.format("//div[@id='broadcast_posts_today']/ul/li[%d]/a", num);
        webDriver.findElementByXPath(mostReadPath).click();
    }


}
