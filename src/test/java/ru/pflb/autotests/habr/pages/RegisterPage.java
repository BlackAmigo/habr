package ru.pflb.autotests.habr.pages;

/**
 * @author Miroshnichenko Anatoliy.
 */
public class RegisterPage extends BasePage {

    public void setEmailField(String email){
        log("Заполняю поле email: " + email);
        String emailFieldPath = "//input[@id='email_field']";
        webDriver.findElementByXPath(emailFieldPath).sendKeys(email);
    }

    public void setNicknameField(String nickname){
        log("Заполняю поле nickname: " + nickname);
        String nicknameFieldPath = "//input[@id='nickname_field']";
        webDriver.findElementByXPath(nicknameFieldPath).sendKeys(nickname);
    }

    public void setPasswordField(String password){
        log("Заполняю поле password: " + password);
        String passwordFieldPath = "//input[@id='password_field']";
        webDriver.findElementByXPath(passwordFieldPath).sendKeys(password);
    }

    public void setPasswordRepeatField(String password){
        log("Заполняю повторно поле password: " + password);
        String passwordRepeatFieldPath = "//input[@id='password_repeat']";
        webDriver.findElementByXPath(passwordRepeatFieldPath).sendKeys(password);
    }

    public void setCaptchaField(String captcha){
        log("Заполняю поле captcha: " + captcha);
        String captchaFieldPath = "//input[@id='captcha']";
        webDriver.findElementByXPath(captchaFieldPath).sendKeys(captcha);
    }

    public void clickRegistrationButton(){
        log("Кликаю по кнопке Регистрация");
        String registrationButtonPath = "//button[@id='registration_button']";
        webDriver.findElementByXPath(registrationButtonPath).click();
    }

    public void isWarningCaptcha(){
        log("Проверяю наличие предупреждения \"Неверно указаны символы с изображения\"");
        String captchaWarningPath = "//div[@class='captcha_widget']//div[@class='s-error']";
        webDriver.isElementPresent(captchaWarningPath);
    }

}
