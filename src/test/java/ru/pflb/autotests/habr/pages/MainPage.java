package ru.pflb.autotests.habr.pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

/**
 * @author Miroshnichenko Anatoliy.
 */
public class MainPage extends BasePage {
    public void open(){
        webDriver.get("http://www.habr.com");
    }

    public void openSearchField(){
        log("Открываю поле поиска");
        String searchButtonPath = "//button[@id='search-form-btn']";
        webDriver.findElementByXPath(searchButtonPath).click();
    }

    public void setSearchField(String query){
        log("Пишу запрос в поле поиска: " + query);
        String searchFieldPath = "//input[@name='q']";
        WebElement searchField = webDriver.findElementByXPath(searchFieldPath);
        searchField.sendKeys(query);
        searchField.sendKeys(Keys.ENTER);
    }

    public void clickRegisterButton(){
        log("Кликаю по кнопке регистрация");
        String registerButtonPath = "//a[@class='btn btn_medium btn_navbar_registration']";
        webDriver.findElementByXPath(registerButtonPath).click();
    }

    public void clickLoginText (){
        log("Кликаю по тексту Логин");
        String loginTextPath = "//div[@class='footer-grid footer-grid_menu']/div[1]/div/ul/li[1]/a";
        webDriver.findElementByXPath(loginTextPath).click();
    }

    public void clickAdvertisingText (){
        log("Кликаю по тексту Реклама");
        String loginTextPath = "//div[@class='footer-grid footer-grid_menu']/div[4]//li[1]/a";
        webDriver.findElementByXPath(loginTextPath).click();
    }
}
