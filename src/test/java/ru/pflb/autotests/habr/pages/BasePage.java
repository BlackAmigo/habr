package ru.pflb.autotests.habr.pages;

import ru.pflb.autotests.habr.WebDriver;

/**
 * @author Miroshnichenko Anatoliy.
 */
public abstract class BasePage {
    WebDriver webDriver = WebDriver.getInstance();

    public void closeAll(){
        webDriver.quit();
    }

    public void log (String msg){
        webDriver.log.trace(msg);
    }
}
