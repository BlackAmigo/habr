package ru.pflb.autotests.habr.pages;

/**
 * @author Miroshnichenko Anatoliy.
 */
public class LoginPage extends BasePage {

    public void setEmailField(String query){
        log("Заполняю поле email: " + query);
        String emailFieldPath = "//input[@id='email_field']";
        webDriver.findElementByXPath(emailFieldPath).sendKeys(query);
    }
    public void setPasswordField(String query){
        log("Заполняю поле password: " + query);
        String passwordFieldPath = "//input[@id='password_field']";
        webDriver.findElementByXPath(passwordFieldPath).sendKeys(query);
    }

    public void isWarningEmail(){
        log("Проверяю наличие предупреждения \"Введите корректный e-mail\"");
        String emailWarningPath = "//div[@class='s-error']";
        webDriver.isElementPresent(emailWarningPath);
    }

}
