package ru.pflb.autotests.habr.pages;

/**
 * @author Miroshnichenko Anatoliy.
 */
public class AdvertisingPage extends BasePage{

    public void isTextPresent(String text){
        log("Проверяю наличие текста \"%s\"" + text);
        String path = String.format("//*[text()='%s']", text);
        String newUrl = ("https://tmtm.ru/services/advertising/");
        webDriver.switchFocusToPage(newUrl);
        webDriver.isElementPresent(path);
    }
}
